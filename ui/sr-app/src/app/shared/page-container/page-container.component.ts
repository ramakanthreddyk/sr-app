import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { PerfectScrollbarComponent } from 'ngx-perfect-scrollbar';

@Component({
    selector: 'app-page-container',
    styleUrls: ['./page-container.component.scss'],
    templateUrl: 'page-container.component.html'
})
export class PageContainerComponent implements AfterViewInit {
    @ViewChild('scrollbar') scrollbar: PerfectScrollbarComponent;

    ngAfterViewInit(): void {
        // scrollbar sometimes not displayed if content was not loaded
        setTimeout(() => this.scrollbar.directiveRef.update(), 200);
    }
}
