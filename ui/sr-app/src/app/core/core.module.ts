import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatToolbarModule } from '@angular/material/toolbar';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TranslateModule } from '@ngx-translate/core';

import { HeaderComponent, NavigationComponent } from './components';

@NgModule({
    declarations: [
        HeaderComponent,
        NavigationComponent
    ],
    imports: [
        CommonModule,
        FlexLayoutModule,
        FontAwesomeModule,
        MatButtonModule,
        MatSelectModule,
        MatToolbarModule,
        RouterModule,
        TranslateModule
    ],
    providers: [],
    exports: [
        HeaderComponent,
        NavigationComponent
    ]
})
export class CoreModule {}
