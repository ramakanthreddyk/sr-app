import { Component, Input } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import {
    faCircle as chartPieLight,
    faHospital as homeLight,
    faFilePdf as filePdfLight
} from '@fortawesome/free-regular-svg-icons';
import {
    faChartPie as chartPieSolid,
    faHospital as homeSolid,
    faFilePdf as filePdfSolid
} from '@fortawesome/free-solid-svg-icons';

import * as fromApp from '../../../app.reducer';

@Component({
    selector: 'app-navigation',
    styleUrls: ['./navigation.component.scss'],
    templateUrl: 'navigation.component.html'
})
export class NavigationComponent {
    @Input() navigationOpen: boolean;
    viewMonthlyReports$: Observable<boolean>;

    icons = {
        chartPieLight,
        chartPieSolid,
        homeLight,
        homeSolid,
        filePdfLight,
        filePdfSolid
    };

    constructor(
        private store: Store<fromApp.ApplicationState>
    ) {
    }
}
