
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { environment } from 'src/environments/environment';
import { faChartBar, faTimesCircle } from '@fortawesome/free-regular-svg-icons';

@Component({
    selector: 'app-header',
    styleUrls: ['./header.component.scss'],
    templateUrl: 'header.component.html'
})
export class HeaderComponent {
    @Input() applicationName = 'Application';
    @Input() navigationOpen: boolean;
    @Input() language: string;
    @Output() readonly selectLanguage: EventEmitter<string> = new EventEmitter<string>();

    languages = environment.languages;

    icons = {
        bars: faChartBar,
        times: faTimesCircle
    };

    onSelectLanguage(language: string) {
        this.selectLanguage.emit(language);
    }
}
