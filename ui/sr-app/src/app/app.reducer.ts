import * as homePage from './home/store/home-page.reducer';
import { ActionReducerMap } from '@ngrx/store';
import { routerReducer, RouterReducerState } from '@ngrx/router-store';

export interface ApplicationState {
    home: homePage.HomePagesState;
    router: RouterReducerState;
}

export const appReducer: ActionReducerMap<ApplicationState> = {
    home: homePage.HomePageReducer,
    router: routerReducer
};
