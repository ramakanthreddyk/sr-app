import { createAction, props } from '@ngrx/store';

export const loadHomepage = createAction(
    '[Home page] Load Home page',
    props<{ }>()
);
