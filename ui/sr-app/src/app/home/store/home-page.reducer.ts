import { createReducer, on } from '@ngrx/store';
import { loadHomepage } from './home-page.action';

export interface HomePagesState {
    loading: boolean;
    loaded: boolean;
}

const initialState: HomePagesState = {
    loading: false,
    loaded: false
};

export const HomePageReducer = createReducer(
    initialState,
    on(loadHomepage, (state, {}) => {
        return { ...state, loading: true, loaded: false };
    }),
);
