import { ofType, Actions, createEffect } from '@ngrx/effects';
import { loadHomepage } from './home-page.action';
import { Injectable } from '@angular/core';

@Injectable()
export class HomePageEffects {
     constructor(
        private actions$: Actions
    ) {}

    loadHomepage$ = createEffect(() =>
        this.actions$.pipe(
            ofType(loadHomepage))
    );
}
