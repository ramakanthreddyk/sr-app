import { Component, OnInit, AfterContentInit, OnDestroy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Subject, BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit, AfterContentInit, OnDestroy {
  private readonly destroy$ = new Subject<void>();
    readonly navigationOpen = new BehaviorSubject<boolean>(false);
  readonly language = new BehaviorSubject<string>(environment.defaultLanguage);

  constructor(
    private translate: TranslateService,
) {
    window.onmessage = (e: MessageEvent): void => {
        if (environment.languages.includes(e.data)) {
            this.translate.use(e.data);
        }
    };
  }

  ngOnInit() {
    this.language.next('en');
    this.translate.use(this.language.value);

    if (window.self !== window.top) {
        const url = new URL(location.href);
        const language = url.searchParams.get('language');
        this.translate.use(language);
    }
  }

  toggleNavigation(navigationOpen: boolean) {
    this.navigationOpen.next(navigationOpen);
}

selectLanguage(language: string) {
    this.language.next(language);
    this.translate.use(language);
}

ngAfterContentInit() {
    // timeout is a workaround solution for IE and Firefox
    setTimeout(() => {
        this.translate.onLangChange.pipe(
            takeUntil(this.destroy$),
            distinctUntilChanged()
        ).subscribe(() => {
            location.reload();
        });
    }, 2000);
}

ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
}
}
