package main

import (
	"log"

	"sr.com/sr.go/api/handler/general"
	"sr.com/sr.go/api/handler/users"
	"sr.com/sr.go/api/openapi"
	"sr.com/sr.go/config"
	"sr.com/sr.go/database"
	userDB "sr.com/sr.go/database/users"
	"sr.com/sr.go/http"
)

func main() {

	log.Println("Starting SR Backend ...")

	conf, err := config.New()
	if err != nil {
		log.Fatalln("Error loading config:", err)
	}

	db := database.New(conf)
	err = db.Connect()
	if err != nil {
		log.Fatalln("Error connecting to database:", err)
	}
	defer db.Close()

	userService := userDB.NewService(db)

	generalHandler := general.NewAPIService(conf)
	userHandler := users.NewAPIService(userService)

	routers := []openapi.Router{
		openapi.NewGeneralApiController(generalHandler),
		openapi.NewUsersApiController(userHandler),
	}

	server := http.New(conf, routers)
	errs := server.Serve()

	log.Fatalln(<-errs)
}
