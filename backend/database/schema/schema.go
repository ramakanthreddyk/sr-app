package schema

import (
	"time"

	"github.com/google/uuid"
	"github.com/jinzhu/gorm"
)

// PrimaryKey is an alias on uint64
type PrimaryKey = uint64

// ForeignKey is an alias on PrimaryKey
type ForeignKey = PrimaryKey

// BaseModel is a struct which is giving some entities some default fields.
type BaseModel struct {
	ID        PrimaryKey `gorm:"type:integer;primary_key"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time
}

// BaseModelUUID is a struct which is giving some entities an uuid field as primary key.
// If the ID field is not set, a new UUIDv1 will be generated before create.
type BaseModelUUID struct {
	ID uuid.UUID `gorm:"primary_key;type:uuid"`
}

// BeforeCreate is used to generate a new uuid for every new entity if the primary key is not set.
func (*BaseModelUUID) BeforeCreate(scope *gorm.Scope) (err error) {
	if scope.PrimaryKeyZero() {
		if id, err := uuid.NewUUID(); err == nil {
			err = scope.SetColumn("ID", id)
		}
	}

	return
}
