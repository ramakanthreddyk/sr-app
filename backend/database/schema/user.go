package schema

// User holds informations of a customer and its pricing plans.
type User struct {
	BaseModelUUID
	LastName  string
	FirstName string
	Email     string
	Mobile    string
	Address   string
}
