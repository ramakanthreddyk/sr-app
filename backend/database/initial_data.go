package database

import (
	"log"

	"github.com/google/uuid"
	"sr.com/sr.go/database/schema"
)

func (database *Database) insertInitialData() {
	log.Println("Add initial data to database ...")

	// insert default user

	user := &schema.User{
		BaseModelUUID: schema.BaseModelUUID{ID: uuid.MustParse("80e3f821-9e5c-11ea-98ed-0205857feb80")},
		LastName:      "last",
		FirstName:     "first",
		Email:         "test@sr.com",
		Mobile:        "12345331455",
		Address:       "Chemnitz",
	}

	var dummy schema.User
	if database.Handle.First(&dummy, &schema.User{BaseModelUUID: user.BaseModelUUID}).RecordNotFound() {
		err := database.Handle.Create(user).Error
		if err != nil {
			panic(err)
		}
	}
}
