package users

import (
	"github.com/google/uuid"
	"sr.com/sr.go/database"
	"sr.com/sr.go/database/schema"
)

// Service represents a service for managing users data.
type Service struct {
	db *database.Database
}

// NewService creates a new instance of a service.
func NewService(db *database.Database) *Service {
	return &Service{
		db: db,
	}
}

// CreateUser creates an new user in the database
func (service *Service) CreateUser(user *schema.User) error {
	return service.db.Handle.Create(user).Error
}

// GetUser returns an user for the given identifier
func (service *Service) GetUser(id uuid.UUID) (*schema.User, error) {
	var user schema.User
	err := service.db.Handle.First(&user, "id = ?", id).Error
	if err != nil {
		return nil, err
	}
	return &user, nil
}

// GetUsers returns all known users
func (service *Service) GetUsers() ([]*schema.User, error) {
	var users []*schema.User
	err := service.db.Handle.Find(&users).Error
	if err != nil {
		return nil, err
	}
	return users, nil
}

func (service *Service) PostUser(user schema.User) (*schema.User, error) {
	err := service.db.Handle.Create(&user).Error
	if err != nil {
		return nil, err
	}
	return &user, nil
}
