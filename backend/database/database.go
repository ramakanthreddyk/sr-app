package database

import (
	"log"
	"net/url"

	"sr.com/sr.go/config"

	"github.com/jinzhu/gorm"
	"sr.com/sr.go/database/schema"
)

// Database defines our connection to the database
type Database struct {
	conf   config.Database
	Handle *gorm.DB
}

// New creates a new Database object
func New(conf *config.Config) *Database {
	return &Database{
		conf: conf.Database,
	}
}

// Connect - opens connection to database and initializes the database
func (database *Database) Connect() error {
	log.Println("Connecting to database ...")

	url, err := url.Parse(database.conf.Connection)
	if err != nil {
		return err
	}

	db, err := gorm.Open(database.conf.Type, url.String())
	if err != nil {
		return err
	}

	db.LogMode(database.conf.Verbose)

	database.Handle = db

	database.initialize()
	database.insertInitialData()
	return nil
}

// Close - close connection to database
func (database *Database) Close() {
	database.Handle.Close()
}

// Initialize database and migrate schemas
func (database *Database) initialize() {
	log.Println("Run automigration on database ...")

	err := database.Handle.AutoMigrate(
		&schema.User{},
	).Error

	if err != nil {
		log.Fatalln("Failed to automigrate the database.", err)
	}
}
