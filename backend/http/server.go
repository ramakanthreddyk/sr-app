package http

import (
	"log"
	"net/http"

	"github.com/gorilla/handlers"
	"sr.com/sr.go/api/openapi"
	"sr.com/sr.go/config"
)

// Server defines our web server

type Server struct {
	conf    config.HTTP
	routers []openapi.Router
}

// New creates a new Server object
func New(conf *config.Config, routers []openapi.Router) *Server {
	return &Server{
		conf:    conf.HTTP,
		routers: routers,
	}
}

// Serve starts the web server
func (server *Server) Serve() chan error {
	router := openapi.NewRouter(server.routers...)

	log.Println("Serving folder", server.conf.AssetsPath, "at /")
	router.PathPrefix("/").Handler(http.FileServer(http.Dir(server.conf.AssetsPath)))

	var handler http.Handler
	if server.conf.CORS != nil {
		handler = handlers.CORS(
			handlers.AllowedOrigins(server.conf.CORS.AllowedOrigins),
			handlers.AllowedMethods(server.conf.CORS.AllowedMethods),
		)(router)
	} else {
		handler = router
	}

	errs := make(chan error)

	go func() {
		// Create the http server
		srv := &http.Server{
			Handler: handler,
			Addr:    server.conf.BindAddress,
		}

		log.Println("Server listening on", srv.Addr)
		if err := srv.ListenAndServe(); err != nil {
			errs <- err
		}
	}()

	return errs
}
