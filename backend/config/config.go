package config

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"

	_ "github.com/mattn/go-sqlite3"
)

// Database holds all configurations for the connection to the database
type Database struct {
	// Type is the type of the database driver
	Type string `json:"type"`

	// Connection is the connection string to connect to the database
	Connection string `json:"connection"`

	// Verbose controls the trace log, e.g. print executes queries
	Verbose bool `json:"verbose"`

	// EncryptionKey is the internal secure key, should not be able to configure via config.json later
	EncryptionKey string `json:"encryption_key"`

	// Passphrase - together with EncryptionKey it creates the key for the encryption of the database
	Passphrase string `json:"passphrase"`
}

// CORS holds all cors specific configurations
type CORS struct {
	// AllowedOrigins is an array of allowed origins to add the cors headers
	AllowedOrigins []string `json:"allowed_origins"`

	// AllowedMethods is an array of allowed methods to add the cors headers
	AllowedMethods []string `json:"allowed_methods"`
}

// HTTP holds all configurations for http server
type HTTP struct {
	// BindAddress is the address to bind the http server
	BindAddress string `json:"bind_address"`

	// CORS defines all cors specific configurations
	CORS *CORS `json:"cors"`

	// AssetsPath is the path where to find the static files (e.g. ui) served by the http server
	AssetsPath string `json:"assets_path"`
}

// Config holds all configurations from config.json
type Config struct {
	Database Database `json:"database"`
	HTTP     HTTP     `json:"http"`
}

func getExecutableFolder() string {
	executable, _ := os.Executable()
	return filepath.Dir(executable)
}

// New generates a new Config object, sets some default values, and loads data from config.json
func New() (*Config, error) {
	config := &Config{
		Database: Database{
			Type:       "sqlite3",
			Connection: "sr.db?_foreign_keys=1",
		},
		HTTP: HTTP{
			BindAddress: ":8080",
			AssetsPath:  filepath.Join(getExecutableFolder(), "public"),
		},
	}

	err := config.Load()
	return config, err
}

// Load loads data from config.json into the config object
func (config *Config) Load() error {
	configFile := "config.json"

	log.Println("Read config", configFile)

	jsonFile, err := ioutil.ReadFile(configFile)
	if err != nil {
		if os.IsNotExist(err) {
			log.Println("Config file does not exist, using default.")
			return nil
		}
		return err
	}

	err = json.Unmarshal(jsonFile, config)
	if err != nil {
		return err
	}

	return nil
}

// IsRootInstance checks if the Synchronisation config is set or not to determine if the instance is root
func (config *Config) IsRootInstance() bool {
	return true
}
