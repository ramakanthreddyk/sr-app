package users

import (
	"github.com/google/uuid"
	"sr.com/sr.go/api/openapi"
	"sr.com/sr.go/database/schema"
	"sr.com/sr.go/database/users"
)

type Handler struct {
	userService *users.Service
}

// NewAPIService creates a default api service
func NewAPIService(userService *users.Service) openapi.UserApiServicer {
	return &Handler{
		userService: userService,
	}
}

func (h *Handler) GetUser(identifier string) (*schema.User, error) {
	uuid, err := uuid.Parse(identifier)
	if err != nil {
		return nil, err
	}

	plan, err := h.userService.GetUser(uuid)
	if err != nil {
		return nil, err
	}

	return plan, nil
}

func (h *Handler) GetUsers() (interface{}, error) {

	plans, err := h.userService.GetUsers()
	if err != nil {
		return nil, err
	}

	return plans, nil
}

func (h *Handler) PostUser(user schema.User) (*schema.User, error) {

	plan, err := h.userService.PostUser(user)
	if err != nil {
		return nil, err
	}

	return plan, nil
}
