package handler

import (
	"net/http"

	"github.com/jinzhu/gorm"
)

// HTTPError is a generic error with a status code and a custom text message
type HTTPError struct {
	StatusCode int
	Text       string
}

func (e HTTPError) Error() string {
	return e.Text
}

// GetStatusFromError return the status and title for a given error
func GetStatusFromError(err error) (int, string) {
	status := http.StatusInternalServerError
	title := err.Error()

	if gorm.IsRecordNotFoundError(err) {
		status = http.StatusNotFound
	} else if he, ok := err.(HTTPError); ok {
		status = he.StatusCode
		title = he.Text
	}

	return status, title
}
