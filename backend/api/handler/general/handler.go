package general

import (
	"sr.com/sr.go/api/openapi"
	"sr.com/sr.go/config"
)

// Handler is a service that implements the logic for the GeneralApiServicer
type Handler struct {
	config *config.Config
}

// NewAPIService creates a default api service
func NewAPIService(config *config.Config) openapi.GeneralApiServicer {
	return &Handler{
		config: config,
	}
}

// GetGeneralSettings - returns the general settings
func (h *Handler) GetGeneralSettings() (interface{}, error) {
	return openapi.GeneralSettings{
		IsRoot: h.config.IsRootInstance(),
	}, nil
}
