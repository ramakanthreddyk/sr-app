package openapi

// GeneralSettings - Object with general settings for application wide usage.
type GeneralSettings struct {
	// To show if the TSSM instance is root or not
	IsRoot bool `json:"is_root"`
}
