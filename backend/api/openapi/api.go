package openapi

import (
	"net/http"

	"sr.com/sr.go/database/schema"
)

// GeneralApiRouter defines the required methods for binding the api requests to a responses for the GeneralApi
// The GeneralApiRouter implementation should parse necessary information from the http request,
// pass the data to a GeneralApiServicer to perform the required actions, then write the service results to the http response.
type GeneralApiRouter interface {
	GetGeneralSettings(http.ResponseWriter, *http.Request)
}

// VersionApiRouter defines the required methods for binding the api requests to a responses for the VersionApi
// The VersionApiRouter implementation should parse necessary information from the http request,
// pass the data to a VersionApiServicer to perform the required actions, then write the service results to the http response.
type VersionApiRouter interface {
	GetVersion(http.ResponseWriter, *http.Request)
}

type UserApiRouter interface {
	GetUser(http.ResponseWriter, *http.Request)
	GetUsers(http.ResponseWriter, *http.Request)
	PostUser(http.ResponseWriter, *http.Request)
}

type UserApiServicer interface {
	GetUser(string) (*schema.User, error)
	GetUsers() (interface{}, error)
	PostUser(schema.User) (*schema.User, error)
}

// GeneralApiServicer defines the api actions for the GeneralApi service
// This interface intended to stay up to date with the openapi yaml used to generate it,
// while the service implementation can ignored with the .openapi-generator-ignore file
// and updated with the logic required for the API.
type GeneralApiServicer interface {
	GetGeneralSettings() (interface{}, error)
}

// VersionApiServicer defines the api actions for the VersionApi service
// This interface intended to stay up to date with the openapi yaml used to generate it,
// while the service implementation can ignored with the .openapi-generator-ignore file
// and updated with the logic required for the API.
type VersionApiServicer interface {
	GetVersion() (interface{}, error)
}
