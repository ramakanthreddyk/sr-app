package openapi

import (
	"encoding/json"
	"net/http"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/gorilla/mux"
	"sr.com/sr.go/api/handler"
	"sr.com/sr.go/database/schema"
)

// use a single instance of Validate
var validate *validator.Validate = validator.New()

// A UsersApiController binds http requests to an api service and writes the service results to the http response
type UsersApiController struct {
	service UserApiServicer
}

// NewUsersApiController creates a default api controller
func NewUsersApiController(s UserApiServicer) Router {
	return &UsersApiController{service: s}
}

// Routes returns all of the api route for the UsersApiController
func (c *UsersApiController) Routes() Routes {
	return Routes{
		{
			"GetUser",
			strings.ToUpper("Get"),
			"/users/{user_identifier}",
			c.GetUser,
		},
		{
			"GetUsers",
			strings.ToUpper("Get"),
			"/users",
			c.GetUsers,
		},
		{
			"PostUser",
			strings.ToUpper("Post"),
			"/users",
			c.PostUser,
		},
	}
}

// GetUser -
func (c *UsersApiController) GetUser(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	userIdentifier := params["user_identifier"]
	result, err := c.service.GetUser(userIdentifier)
	if err != nil {
		status, title := handler.GetStatusFromError(err)
		EncodeJSONProblemResponse(status, title, w)
		return
	}

	EncodeJSONResponse(result, nil, w)
}

// GetUsers -
func (c *UsersApiController) GetUsers(w http.ResponseWriter, r *http.Request) {
	result, err := c.service.GetUsers()
	if err != nil {
		status, title := handler.GetStatusFromError(err)
		EncodeJSONProblemResponse(status, title, w)
		return
	}

	EncodeJSONResponse(result, nil, w)
}

// Postuser -
func (c *UsersApiController) PostUser(w http.ResponseWriter, r *http.Request) {
	user := &schema.User{}
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		EncodeJSONProblemResponse(http.StatusBadRequest, err.Error(), w)
		return
	}

	if err := validate.Struct(user); err != nil {
		EncodeJSONProblemResponse(http.StatusBadRequest, err.Error(), w)
		return
	}

	_, err := c.service.PostUser(*user)
	if err != nil {
		status, title := handler.GetStatusFromError(err)
		EncodeJSONProblemResponse(status, title, w)
		return
	}

	w.WriteHeader(http.StatusCreated)
}
