package openapi

import (
	"net/http"
	"strings"

	"sr.com/sr.go/api/handler"
)

// A GeneralApiController binds http requests to an api service and writes the service results to the http response
type GeneralApiController struct {
	service GeneralApiServicer
}

// NewGeneralApiController creates a default api controller
func NewGeneralApiController(s GeneralApiServicer) Router {
	return &GeneralApiController{service: s}
}

// Routes returns all of the api route for the GeneralApiController
func (c *GeneralApiController) Routes() Routes {
	return Routes{
		{
			"GetGeneralSettings",
			strings.ToUpper("Get"),
			"/general-settings",
			c.GetGeneralSettings,
		},
	}
}

// GetGeneralSettings -
func (c *GeneralApiController) GetGeneralSettings(w http.ResponseWriter, r *http.Request) {
	result, err := c.service.GetGeneralSettings()
	if err != nil {
		status, title := handler.GetStatusFromError(err)
		EncodeJSONProblemResponse(status, title, w)
		return
	}

	EncodeJSONResponse(result, nil, w)
}
