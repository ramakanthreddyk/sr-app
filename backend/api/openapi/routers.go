package openapi

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

// A Route defines the parameters for an api endpoint
type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

// Routes are a collection of defined api endpoints
type Routes []Route

// Router defines the required methods for retrieving api routes
type Router interface {
	Routes() Routes
}

// NewRouter creates a new router for any number of api routers
func NewRouter(routers ...Router) *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	for _, api := range routers {
		for _, route := range api.Routes() {
			var handler http.Handler
			handler = route.HandlerFunc
			handler = Logger(handler, route.Name)

			router.
				PathPrefix("/api").
				Methods(route.Method).
				Path(route.Pattern).
				Name(route.Name).
				Handler(handler)
		}
	}

	return router
}

// EncodeJSONResponse uses the json encoder to write an interface to the http response with an optional status code
func EncodeJSONResponse(i interface{}, status *int, w http.ResponseWriter) error {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if status != nil {
		w.WriteHeader(*status)
	} else {
		w.WriteHeader(http.StatusOK)
	}

	return json.NewEncoder(w).Encode(i)
}

// EncodeJSONProblemResponse uses the json encoder to write an interface to the http response with an optional status code
func EncodeJSONProblemResponse(status int, title string, w http.ResponseWriter) error {
	problem := Problem{
		Status: int32(status),
		Title:  title,
	}

	w.Header().Set("Content-Type", "application/problem+json; charset=UTF-8")
	w.WriteHeader(status)
	return json.NewEncoder(w).Encode(problem)
}

// ReadFormFileToBuffer reads file data from a request form and writes it to a temporary file
func ReadFormFileToBuffer(r *http.Request, key string) ([]byte, error) {
	r.ParseForm()
	formFile, _, err := r.FormFile(key)
	if err != nil {
		return nil, err
	}

	defer formFile.Close()

	fileBytes, err := ioutil.ReadAll(formFile)
	if err != nil {
		return nil, err
	}

	return fileBytes, nil
}

// parseIntParameter parses a sting parameter to an int64
func parseIntParameter(param string) (int64, error) {
	return strconv.ParseInt(param, 10, 64)
}
